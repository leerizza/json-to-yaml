import json

from ruamel_yaml import YAML
from ruamel_yaml.compat import with_metaclass

in_file = "flow_Profiling  Quality Rules Processing.json"
out_file = "flow_Profiling  Quality Rules Processing.yaml"

yaml = YAML (typ="safe")

with open(in_file, "r", encoding="utf-8") as i:
    data = yaml.load(i)

with open(out_file, "w", encoding="utf-8") as o:
    yaml.dump(data, o)