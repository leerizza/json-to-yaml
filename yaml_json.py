import json

from ruamel_yaml import YAML

in_file = "flow_Profiling  Quality Rules Processing.yaml"
out_file = "flow_Profiling  Quality Rules Processing_new2.json"

yaml = YAML (typ="safe")

with open(in_file, "r", encoding="utf-8") as i:
    data = yaml.load(i)

with open(out_file, "w", encoding="utf-8") as o:
    json.dump(data, o, indent=4, ensure_ascii=False)